# CPAH: Cyberpunk 2077 Autohacker

Test change 2

[Click here for the full documentation](https://jkchen2.gitlab.io/cpah/)

This is a tool to help make the Cyberpunk 2077 Breach Protocol hacking minigame less tedious.
Check out the video below for a quick demonstration:

![demo](docs/media/demo.mp4)
